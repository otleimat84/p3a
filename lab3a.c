/*NAME: Omar Tleimat,Kevin Church
ID: 404844706,904670893
EMAIL: otleimat@ucla.edu,kchurch@ucla.edu*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <sys/stat.h>
#include <assert.h>
#include <math.h>
#include <time.h>
#include "ext2_fs.h"

// offset for the base
#define OFFSET 1024
#define BITS_IN_BYTE 8

struct ext2_super_block sb;	
// Descriptor for the single group
struct ext2_group_desc gd;		

//Define the directory struct
struct ext2_dir_entry_2 {
  __u32 inode;/* Inode number */
  __u16 rec_len;/* Directory entry length */
  __u8 name_len;/* Name length */
  __u8 file_type;
  char name[EXT2_NAME_LEN];/* File name */
};
  

// file descriptor for image file
int fd;  
unsigned int block_size = 0; 
unsigned int blocks_per_group = 0;
unsigned int inodes_per_group = 0;
unsigned int total_blocks = 0;
unsigned int total_inodes = 0;
int first_offset = 12;
uint32_t *inode_bitmap = NULL;
unsigned int inode_table_block = 0;


void set_time_format(uint32_t time, char* dest){
	time_t t = time;
	struct tm tmp = *gmtime(&t);
	strftime(dest, 128, "%m/%d/%y %H:%M:%S", &tmp);
}

void erorr_exit(const char* msg){
	fprintf(stderr, "%s\n", msg);
	// All errors outside of input file should exit with code 2
	exit(2);
}

unsigned int get_offset(unsigned int block_num){
    return OFFSET + (block_num - 1) * block_size;
}

void process_bitmap(uint32_t bitmap, const char *msg) {
	int mask;
	for(unsigned long i = 0; i < block_size; i++){	
		// Get one byte
		uint8_t curr_byte;
		if (pread(fd, &curr_byte, 1, (block_size * bitmap) + i) < 0)
			erorr_exit("pread error");
		// If we're scanning inode_bitmap, update indicies with the current 
		// byte to use later while scanning inodes
		if (inode_bitmap)
			inode_bitmap[i] = curr_byte;
		mask = 1;
		// From each read byte from pread, examine it bit by bit
		for(unsigned int j = 0; j < BITS_IN_BYTE; j++){
			if((curr_byte & mask) == 0)
				printf("%s,%lu\n", msg, i * BITS_IN_BYTE + j + 1);
			mask <<= 1;
		}
	}
}

void process_inode_bitmap(){
	inode_bitmap = malloc(sizeof(uint8_t) * block_size);
	process_bitmap(gd.bg_inode_bitmap, "IFREE");
}

void process_block_bitmap(){
	process_bitmap(gd.bg_block_bitmap, "BFREE");
}

void process_superblock(){
	if (pread(fd, &sb, sizeof(struct ext2_super_block), OFFSET) < 0) {
		erorr_exit("pread error while getting superblock");
	}
	if (sb.s_magic != EXT2_SUPER_MAGIC) {
		erorr_exit("superblock magic error");
	}
	//  Logic taken from header file
	block_size = EXT2_MIN_BLOCK_SIZE << sb.s_log_block_size,
	printf("SUPERBLOCK,%u,%u,%u,%u,%u,%u,%u\n",
			sb.s_blocks_count,
			sb.s_inodes_count,
			block_size,
			sb.s_inode_size,
			sb.s_blocks_per_group,
			sb.s_inodes_per_group,
			sb.s_first_ino);

	blocks_per_group = sb.s_blocks_per_group;
	inodes_per_group = sb.s_inodes_per_group,
	total_blocks = sb.s_blocks_count;
	total_inodes = sb.s_inodes_count;
}

void process_blockgroup(){
	unsigned int group_number;
	unsigned int blocks_in_group = blocks_per_group;
	unsigned int inodes_in_group = inodes_per_group;
	unsigned int total_groups = total_blocks / blocks_per_group;

  	if( total_blocks % blocks_per_group ) //If there is a partitally filled group
    	total_groups++;
  
  	for(group_number = 0; group_number < total_groups; group_number++)
    {
      	if (pread(fd, &gd, sizeof(struct ext2_group_desc), OFFSET + (1 + group_number) * block_size) < 0) {
			erorr_exit("pread error while reading group");
      	}
      	if (group_number == total_groups - 1){ //If the last group
			blocks_in_group = total_blocks % blocks_per_group;
			inodes_in_group = total_inodes % inodes_per_group;
			if(!inodes_in_group)
			  	inodes_in_group = inodes_per_group;
		}
     	 printf("GROUP,%u,%u,%u,%u,%u,%u,%u,%u\n",
		    group_number,
		    blocks_in_group,
		    inodes_in_group,
		    gd.bg_free_blocks_count,
		    gd.bg_free_inodes_count,
		    gd.bg_block_bitmap,
		    gd.bg_inode_bitmap,
		    gd.bg_inode_table);
    }
	inode_table_block = gd.bg_inode_table;
}

void process_indirect(int num, int block, int offset, int recursion_level, int called){
	if (recursion_level == 0)
		return;
	int * indirects = malloc(block_size);
	if (pread(fd, indirects, block_size, block * block_size) < 0){
		erorr_exit("pread error while reading indirect blocks");
	}
	for(unsigned int j = 0; j < block_size/4; j++){	
		if (indirects[j] != 0){
			if (!called && recursion_level == 1){
				offset = first_offset;
			}
			printf("INDIRECT,%u,%d,%d,%u,%u\n", num, recursion_level, offset, block, indirects[j]);
			if (recursion_level == 1)
				first_offset++;
			else if (recursion_level == 2)
				offset += 2;
			process_indirect(num, indirects[j], offset, recursion_level-1, 1);
		}
	}
}

void process_directory(struct ext2_inode inode_dir, unsigned int inode_num){
	struct ext2_dir_entry_2 dir_entry;
	unsigned int curr_offset = 0;
	char name_buf [EXT2_NAME_LEN + 1] = {0};

	//Stop reading the i_block entry when no more files are left
	while(curr_offset < block_size){
	    if (pread(fd, &dir_entry, sizeof(struct ext2_dir_entry_2),
		  (int) get_offset(inode_dir.i_block[0]) + curr_offset) < 0) 
	    {
	      	erorr_exit("pread error while reading directory");
	    }
	    strncpy(name_buf, dir_entry.name, dir_entry.name_len);
	    name_buf[dir_entry.name_len] = 0;
	    printf("DIRENT,%u,%u,%u,%u,%u,\'%s\'\n",
		   inode_num,
		   curr_offset,
		   dir_entry.inode,
		   dir_entry.rec_len,
		   dir_entry.name_len,
		   name_buf);
	    curr_offset += dir_entry.rec_len;
	}
}

int is_valid_inode(unsigned int no){
	// skip if root (2) or if the number is less than first reserved inode
	if (no < sb.s_first_ino && no != 2){
		//printf("No: %d is less than first reserved, %s\n", no,"returning");
		return 0;
	}
	int mask;
	uint8_t curr_byte;
	for (unsigned long i = 0 ; i < block_size; i++) {
		curr_byte = inode_bitmap[i];
		mask = 1;
		for (int j = 0; j < BITS_IN_BYTE; j++) {
			// keep going till we hit the appropiate inode no
			if ((i * BITS_IN_BYTE + j + 1) == no)
				// We only want inodes that have been allocated
				return (curr_byte & mask) != 0;
			mask <<= 1;
		}
	}
	return 0;
}

void process_inodes(){
	unsigned int curr_inode_offset = get_offset(inode_table_block);
	struct ext2_inode curr_inode;
	//struct ext2_dir_entry_2 dir;
	char file_type;
	__u16 inode_mode_mask = 0b111111111111;
	char ctime_buf [128], mtime_buf [128], atime_buf [128];
	// Scan through all inodes
  	for (unsigned int i = 1; i <= total_inodes; i++){
  		// Only process valid and non-free inodes
    	if (is_valid_inode(i)){
		    if (pread(fd, &curr_inode, sizeof(struct ext2_inode), curr_inode_offset) < 0) {
		        erorr_exit("pread error while processing inodes");
		    }
		    //Using Macros to check file type of inode
		    if(S_ISREG(curr_inode.i_mode))
		        file_type = 'f';
		    else if (S_ISDIR(curr_inode.i_mode)){
			    file_type = 'd';
			    process_directory(curr_inode, i);
		    }
		    else if (S_ISLNK(curr_inode.i_mode))
		        file_type = 's';
		    else
		        file_type = '?';
		  	set_time_format(curr_inode.i_mtime, mtime_buf);
		  	set_time_format(curr_inode.i_ctime, ctime_buf);
		  	set_time_format(curr_inode.i_atime, atime_buf);
		  	// single indirect blocks
		  	if (curr_inode.i_block[12] != 0){
		  		process_indirect(i, curr_inode.i_block[12], 12, 1, 0);
		  	}
		  	// doubly indirect blocks
		  	if (curr_inode.i_block[13] != 0){
		  		process_indirect(i, curr_inode.i_block[13], 268, 2, 0);
		  	}
		  	// triply indirects blocks
		  	if (curr_inode.i_block[14] != 0){
		  		process_indirect(i, curr_inode.i_block[14], 65804, 3, 0);
		  	}

		    printf("INODE,%u,%c,%o,%u,%u,%u,%s,%s,%s,%u,%u,"
			   "%u,%u,%u,%u,%u,%u,%u,%u,%u,%u,%u,%u,%u,%u,%u\n",
			   i,
			   file_type,
			   curr_inode.i_mode & inode_mode_mask,
			   curr_inode.i_uid,
			   curr_inode.i_gid,
			   curr_inode.i_links_count,
			   ctime_buf,
			   mtime_buf,
			   atime_buf,
			   curr_inode.i_size,
			   curr_inode.i_blocks,
			   curr_inode.i_block[0],
			   curr_inode.i_block[1],
			   curr_inode.i_block[2],
			   curr_inode.i_block[3],
			   curr_inode.i_block[4],
			   curr_inode.i_block[5],
			   curr_inode.i_block[6],
			   curr_inode.i_block[7],
			   curr_inode.i_block[8],
			   curr_inode.i_block[9],
			   curr_inode.i_block[10],
			   curr_inode.i_block[11],
			   curr_inode.i_block[12],
			   curr_inode.i_block[13],
			   curr_inode.i_block[14]);
		}
	//Move offset to the next inode
    curr_inode_offset += sizeof(struct ext2_inode);    
  }
}

void process_all_items(){
	// Scans must be done in this order
	process_superblock();
	process_blockgroup();
	process_block_bitmap();
	process_inode_bitmap();
	process_inodes();
}

int main(int argc, char **argv){
	if (argc != 2) {
        printf("Usage: ./lab3a img_file\n");
    }
    const char *file_name = argv[1];
    fd = open(file_name, O_RDONLY);
    if (fd < 0)
    {
    	// Gracefully end program with error message if image file provided threw an error 
    	fprintf(stderr, "Error with input file '%s': %s\n", file_name, strerror(errno));
        exit(1);
    }
	process_all_items();
	exit(0); 
}
