# NAME: Omar Tleimat,Kevin Church
# ID: 404844706,904670893
# EMAIL: otleimat@ucla.edu,kchurch@ucla.edu

all_files = lab3a-404844706.tar.gz Makefile README ext2_fs.h lab3a.c

.SILENT: 

default: 
	gcc -g -std=gnu99 -Wall -Wextra -o lab3a lab3a.c

clean: 
	rm -f lab3a-404844706.tar.gz lab3a 

dist:
	tar -czvf $(all_files)
